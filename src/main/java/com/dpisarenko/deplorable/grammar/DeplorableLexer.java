// Generated from com/dpisarenko/deplorable/grammar/Deplorable.g4 by ANTLR 4.9.1
package com.dpisarenko.deplorable.grammar;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DeplorableLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, LPAREN=4, RPAREN=5, DEPLORABLE_IDENTIFIER=6, DEPLORABLE_STRING=7, 
		CURLY_BRACE_LEFT=8, CURLY_BRACE_RIGHT=9, NEW_LINE=10, DEPLORABLE_NUMBER=11, 
		WHITESPACE=12;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "LPAREN", "RPAREN", "DEPLORABLE_IDENTIFIER", 
			"DEPLORABLE_STRING", "COLON", "BACKSLASH", "PERCENT_SIGN", "SINGLE_QUOTE", 
			"CURLY_BRACE_LEFT", "CURLY_BRACE_RIGHT", "NEW_LINE", "DEPLORABLE_NUMBER", 
			"COMMA", "DASH", "LOWERCASE_LATIN_LETTER", "UPPERCASE_LATIN_LETTER", 
			"UNDERSCORE", "WHITESPACE", "EXCLAMATION_POINT", "DIGIT"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "';'", "','", "'@MetaX'", "'('", "')'", null, null, "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, "LPAREN", "RPAREN", "DEPLORABLE_IDENTIFIER", 
			"DEPLORABLE_STRING", "CURLY_BRACE_LEFT", "CURLY_BRACE_RIGHT", "NEW_LINE", 
			"DEPLORABLE_NUMBER", "WHITESPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public DeplorableLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Deplorable.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\16\u008a\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2"+
		"\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3"+
		"\7\6\7E\n\7\r\7\16\7F\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\6\bV\n\b\r\b\16\bW\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r"+
		"\3\r\3\16\3\16\3\17\3\17\5\17j\n\17\3\17\5\17m\n\17\3\17\3\17\3\20\6\20"+
		"r\n\20\r\20\16\20s\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3"+
		"\26\6\26\u0081\n\26\r\26\16\26\u0082\3\26\3\26\3\27\3\27\3\30\3\30\2\2"+
		"\31\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\2\23\2\25\2\27\2\31\n\33\13\35\f"+
		"\37\r!\2#\2%\2\'\2)\2+\16-\2/\2\3\2\3\4\2\13\13\"\"\2\u0092\2\3\3\2\2"+
		"\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3"+
		"\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2+\3\2\2\2"+
		"\3\61\3\2\2\2\5\63\3\2\2\2\7\65\3\2\2\2\t<\3\2\2\2\13>\3\2\2\2\rD\3\2"+
		"\2\2\17H\3\2\2\2\21[\3\2\2\2\23]\3\2\2\2\25_\3\2\2\2\27a\3\2\2\2\31c\3"+
		"\2\2\2\33e\3\2\2\2\35l\3\2\2\2\37q\3\2\2\2!u\3\2\2\2#w\3\2\2\2%y\3\2\2"+
		"\2\'{\3\2\2\2)}\3\2\2\2+\u0080\3\2\2\2-\u0086\3\2\2\2/\u0088\3\2\2\2\61"+
		"\62\7=\2\2\62\4\3\2\2\2\63\64\7.\2\2\64\6\3\2\2\2\65\66\7B\2\2\66\67\7"+
		"O\2\2\678\7g\2\289\7v\2\29:\7c\2\2:;\7Z\2\2;\b\3\2\2\2<=\7*\2\2=\n\3\2"+
		"\2\2>?\7+\2\2?\f\3\2\2\2@E\5%\23\2AE\5\'\24\2BE\5)\25\2CE\5#\22\2D@\3"+
		"\2\2\2DA\3\2\2\2DB\3\2\2\2DC\3\2\2\2EF\3\2\2\2FD\3\2\2\2FG\3\2\2\2G\16"+
		"\3\2\2\2HU\7$\2\2IV\5%\23\2JV\5\'\24\2KV\5)\25\2LV\7\"\2\2MV\5-\27\2N"+
		"V\5\21\t\2OV\5\23\n\2PV\5\13\6\2QV\5\t\5\2RV\5!\21\2SV\5\25\13\2TV\5\27"+
		"\f\2UI\3\2\2\2UJ\3\2\2\2UK\3\2\2\2UL\3\2\2\2UM\3\2\2\2UN\3\2\2\2UO\3\2"+
		"\2\2UP\3\2\2\2UQ\3\2\2\2UR\3\2\2\2US\3\2\2\2UT\3\2\2\2VW\3\2\2\2WU\3\2"+
		"\2\2WX\3\2\2\2XY\3\2\2\2YZ\7$\2\2Z\20\3\2\2\2[\\\7<\2\2\\\22\3\2\2\2]"+
		"^\7^\2\2^\24\3\2\2\2_`\7\'\2\2`\26\3\2\2\2ab\7)\2\2b\30\3\2\2\2cd\7}\2"+
		"\2d\32\3\2\2\2ef\7\177\2\2f\34\3\2\2\2gi\7\17\2\2hj\7\f\2\2ih\3\2\2\2"+
		"ij\3\2\2\2jm\3\2\2\2km\7\f\2\2lg\3\2\2\2lk\3\2\2\2mn\3\2\2\2no\b\17\2"+
		"\2o\36\3\2\2\2pr\5/\30\2qp\3\2\2\2rs\3\2\2\2sq\3\2\2\2st\3\2\2\2t \3\2"+
		"\2\2uv\7.\2\2v\"\3\2\2\2wx\7/\2\2x$\3\2\2\2yz\4c|\2z&\3\2\2\2{|\4C\\\2"+
		"|(\3\2\2\2}~\7a\2\2~*\3\2\2\2\177\u0081\t\2\2\2\u0080\177\3\2\2\2\u0081"+
		"\u0082\3\2\2\2\u0082\u0080\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0084\3\2"+
		"\2\2\u0084\u0085\b\26\2\2\u0085,\3\2\2\2\u0086\u0087\7#\2\2\u0087.\3\2"+
		"\2\2\u0088\u0089\4\62;\2\u0089\60\3\2\2\2\13\2DFUWils\u0082\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}