// Generated from com/dpisarenko/deplorable/grammar/Deplorable.g4 by ANTLR 4.9.1
package com.dpisarenko.deplorable.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link DeplorableParser}.
 */
public interface DeplorableListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#script}.
	 * @param ctx the parse tree
	 */
	void enterScript(DeplorableParser.ScriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#script}.
	 * @param ctx the parse tree
	 */
	void exitScript(DeplorableParser.ScriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(DeplorableParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(DeplorableParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void enterMethodCall(DeplorableParser.MethodCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void exitMethodCall(DeplorableParser.MethodCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#methodName}.
	 * @param ctx the parse tree
	 */
	void enterMethodName(DeplorableParser.MethodNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#methodName}.
	 * @param ctx the parse tree
	 */
	void exitMethodName(DeplorableParser.MethodNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#methodArgument}.
	 * @param ctx the parse tree
	 */
	void enterMethodArgument(DeplorableParser.MethodArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#methodArgument}.
	 * @param ctx the parse tree
	 */
	void exitMethodArgument(DeplorableParser.MethodArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#functionStatement}.
	 * @param ctx the parse tree
	 */
	void enterFunctionStatement(DeplorableParser.FunctionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#functionStatement}.
	 * @param ctx the parse tree
	 */
	void exitFunctionStatement(DeplorableParser.FunctionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(DeplorableParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(DeplorableParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void enterFunctionBody(DeplorableParser.FunctionBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void exitFunctionBody(DeplorableParser.FunctionBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link DeplorableParser#metaX}.
	 * @param ctx the parse tree
	 */
	void enterMetaX(DeplorableParser.MetaXContext ctx);
	/**
	 * Exit a parse tree produced by {@link DeplorableParser#metaX}.
	 * @param ctx the parse tree
	 */
	void exitMetaX(DeplorableParser.MetaXContext ctx);
}