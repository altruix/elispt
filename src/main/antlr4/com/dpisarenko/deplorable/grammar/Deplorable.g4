grammar Deplorable;

script: statement*;
statement: (methodCall | functionDeclaration) ';';

// General stuff

// Method call definition
methodCall: methodName LPAREN (methodArgument ','?)* RPAREN;

methodName: DEPLORABLE_IDENTIFIER;
methodArgument: (DEPLORABLE_STRING | DEPLORABLE_NUMBER | DEPLORABLE_IDENTIFIER);

// Function Declaration
functionStatement: methodCall ';';
functionDeclaration: metaX? methodName LPAREN (methodArgument ','?)* RPAREN functionBody;
functionBody: CURLY_BRACE_LEFT functionStatement* CURLY_BRACE_RIGHT;

// @MetaX Annotation
metaX: '@MetaX' LPAREN DEPLORABLE_STRING? RPAREN;

// Lexer stuff
LPAREN: '(';
RPAREN: ')';
DEPLORABLE_IDENTIFIER: (
        LOWERCASE_LATIN_LETTER
        | UPPERCASE_LATIN_LETTER
        | UNDERSCORE
        | DASH
    )+;
DEPLORABLE_STRING: '"' (
        LOWERCASE_LATIN_LETTER
        | UPPERCASE_LATIN_LETTER
        | UNDERSCORE
        | ' '
        | EXCLAMATION_POINT
        | COLON
        | BACKSLASH
        | RPAREN
        | LPAREN
        | COMMA
        | PERCENT_SIGN
        | SINGLE_QUOTE
    )+ '"';

fragment COLON: ':';
fragment BACKSLASH: '\\';
fragment PERCENT_SIGN: '%';
fragment SINGLE_QUOTE: '\'';

CURLY_BRACE_LEFT: '{';
CURLY_BRACE_RIGHT: '}';

NEW_LINE: (
    '\r' '\n'?
    | '\n'
) -> skip;

DEPLORABLE_NUMBER: DIGIT+;

fragment COMMA: ',';

fragment DASH: '-';
fragment LOWERCASE_LATIN_LETTER: 'a'..'z';
fragment UPPERCASE_LATIN_LETTER: 'A'..'Z';
fragment UNDERSCORE: '_';
WHITESPACE: [ \t]+ -> skip;
fragment EXCLAMATION_POINT: '!';
fragment DIGIT: '0'..'9';