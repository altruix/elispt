package com.dpisarenko.deplorable.model

data class MethodCall(
    val methodName:String,
    val arguments:List<AbstractMethodCallArgument>
) : DeplorableStatement