package com.dpisarenko.deplorable.model

data class FunctionDefinition(
    val functionName:String,
    val parameterNames:List<String>,
    val body:List<DeplorableStatement>,
    val metaX:Boolean = false,
    val metaXText:String = ""
) : DeplorableStatement {
}