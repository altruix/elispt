package com.dpisarenko.deplorable

import com.dpisarenko.deplorable.model.*
import java.lang.IllegalArgumentException
import java.util.stream.Collectors

class EmacsLispCodeGenerator(val deplorableStatements:List<DeplorableStatement>) {
    val TAB:String = "   ";
    fun transformToEmacsLisp(): String {
        return deplorableStatements.stream()
            .map { deplorableStatement -> convertToEmacsLisp(deplorableStatement) }
            .collect(Collectors.joining(System.lineSeparator()))
    }

    private fun convertToEmacsLisp(deplorableStatement:DeplorableStatement):String {
        if (deplorableStatement is MethodCall) {
            return convertDeplorableMethodCall(deplorableStatement)
        }
        if (deplorableStatement is FunctionDefinition) {
            return convertDeplorableFunctionDefinition(deplorableStatement)
        }
        return ""
    }

    private fun convertDeplorableFunctionDefinition(functionDefinition: FunctionDefinition): String {
        val sb = StringBuilder()
        sb.append("(defun ") // Top-level (root) parenthesis
        sb.append(functionDefinition.functionName)
        if (functionDefinition.parameterNames.isNotEmpty()) {
            sb.append(" (")
            var first = true
            for (curParameter in functionDefinition.parameterNames) {
                if (first) {
                    first = false
                } else {
                    sb.append(" ")
                }
                sb.append(curParameter)
            }
            sb.append(")")
        } else {
            sb.append(" ()")
        }
        sb.append(System.lineSeparator())
        if (functionDefinition.metaX) {
            sb.append(TAB)
            sb.append("\"Nonce function\"")
            sb.append(System.lineSeparator())
            sb.append(TAB)
            sb.append("(interactive")
            if (!functionDefinition.metaXText.isNullOrBlank()) {
                sb.append(" ")
                sb.append(functionDefinition.metaXText)
            }
            sb.append(")")
            sb.append(System.lineSeparator())
        }
        for (stmt in functionDefinition.body) {
            if (stmt is MethodCall) {
                sb.append(TAB)
                sb.append(convertDeplorableMethodCall(stmt))
                sb.append(System.lineSeparator())
            } else {
                throw IllegalArgumentException("We currently don't support anything except method calls inside functions")
            }
        }
        sb.append(")") // Top-level (root) parenthesis
        return sb.toString()
    }

    private fun convertDeplorableMethodCall(methodCall: MethodCall): String {
        val sb = StringBuilder()

        sb.append("(")
        sb.append(methodCall.methodName)
        var первыйНах = true
        for (arg in methodCall.arguments) {
            if (первыйНах) {
                первыйНах = false
                sb.append(" ")
            } else {
                sb.append(" ")
            }
            if (arg is StringMethodCallArgument) {
                sb.append(arg.value)
            } else if (arg is IntegerMethodCallArgument) {
                sb.append(arg.value.toString())
            } else if (arg is IdentifierMethodCallArgument) {
                sb.append(arg.value)
            }
        }
        sb.append(")")
        return sb.toString()
    }
}