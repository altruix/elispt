package com.dpisarenko.deplorable

import com.dpisarenko.deplorable.grammar.DeplorableParser
import com.dpisarenko.deplorable.model.*
import java.lang.IllegalArgumentException

class DeplorableScriptListener : DeplorableListenerAdapter() {
    private val deplorableStatements:MutableList<DeplorableStatement> = ArrayList()
    private var weAreInsideFunctionDefinitionNow = false

    val readDeplorableStatements:List<DeplorableStatement>
        get() = deplorableStatements

    override fun exitMethodCall(ctx: DeplorableParser.MethodCallContext?) {
        if (!weAreInsideFunctionDefinitionNow) {
            val methodCall = transformMethodCall(ctx)
            deplorableStatements.add(methodCall)
        }
    }

    private fun transformMethodCall(ctx: DeplorableParser.MethodCallContext?): MethodCall {
        val methodName = ctx!!.methodName().text
        val arguments: MutableList<AbstractMethodCallArgument> = ArrayList()
        for (arg in ctx.methodArgument()) {
            val deplorableString = arg.DEPLORABLE_STRING()
            if (deplorableString != null) {
                val value = deplorableString.text
                val stringMethodCallArgument = StringMethodCallArgument(value)
                arguments.add(stringMethodCallArgument)
            }

            val deplorableInteger = arg.DEPLORABLE_NUMBER()
            if (deplorableInteger != null) {
                val value = Integer.parseInt(deplorableInteger.text)
                val integerMethodCallArgument = IntegerMethodCallArgument(value)
                arguments.add(integerMethodCallArgument)
            }

            val deplorableIdentifier = arg.DEPLORABLE_IDENTIFIER()
            if (deplorableIdentifier != null) {
                val value = deplorableIdentifier.text
                val identifierMethodCallArgument = IdentifierMethodCallArgument(value)
                arguments.add(identifierMethodCallArgument)
            }
        }
        return MethodCall(methodName, arguments)
    }

    override fun enterFunctionStatement(ctx: DeplorableParser.FunctionStatementContext?) {
        weAreInsideFunctionDefinitionNow = true
    }

    override fun exitFunctionDeclaration(ctx: DeplorableParser.FunctionDeclarationContext?) {
        val methodName = ctx!!.methodName().DEPLORABLE_IDENTIFIER().text
        val parameterNames:MutableList<String> = ArrayList()
        if (ctx.methodArgument().size != 0) {
            for (curArg in ctx.methodArgument()) {
                if (curArg.DEPLORABLE_IDENTIFIER() != null) {
                    parameterNames.add(curArg.DEPLORABLE_IDENTIFIER().text)
                } else {
                    throw IllegalArgumentException("""Only identifiers are allowed in
                        function declarations and of the parameters of $methodName is not.""".trimIndent())
                }
            }
        }
        val transformedFunctionStatements:MutableList<DeplorableStatement> = ArrayList()

        val functionBody = ctx.functionBody()
        val functionStatements = functionBody.functionStatement()
        for (curStatement in functionStatements) {
            val methodCallCtx = curStatement.methodCall()
            if (methodCallCtx != null) {
                val methodCall = transformMethodCall(methodCallCtx)
                transformedFunctionStatements.add(methodCall)
            }
        }
        var metaX = false
        var metaXText = ""
        if (ctx.metaX() != null) {
            metaX = true
            if (ctx.metaX().DEPLORABLE_STRING() != null) {
                metaXText = ctx.metaX().DEPLORABLE_STRING().text
            }
        }
        val transformedFunctionDefinition = FunctionDefinition(
            methodName,
            parameterNames,
            transformedFunctionStatements,
            metaX,
            metaXText
        )
        deplorableStatements.add(transformedFunctionDefinition)
        weAreInsideFunctionDefinitionNow = false
    }
}