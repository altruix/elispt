package com.dpisarenko.deplorable

import com.dpisarenko.deplorable.grammar.DeplorableListener
import com.dpisarenko.deplorable.grammar.DeplorableParser
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ErrorNode
import org.antlr.v4.runtime.tree.TerminalNode

open class DeplorableListenerAdapter : DeplorableListener {
    override fun visitTerminal(p0: TerminalNode?) {
    }

    override fun visitErrorNode(p0: ErrorNode?) {
    }

    override fun enterEveryRule(p0: ParserRuleContext?) {
    }

    override fun exitEveryRule(p0: ParserRuleContext?) {
    }

    override fun enterScript(ctx: DeplorableParser.ScriptContext?) {
    }

    override fun exitScript(ctx: DeplorableParser.ScriptContext?) {
    }

    override fun enterStatement(ctx: DeplorableParser.StatementContext?) {
    }

    override fun exitStatement(ctx: DeplorableParser.StatementContext?) {
    }

    override fun enterMethodCall(ctx: DeplorableParser.MethodCallContext?) {
    }

    override fun exitMethodCall(ctx: DeplorableParser.MethodCallContext?) {
    }

    override fun enterMethodName(ctx: DeplorableParser.MethodNameContext?) {
    }

    override fun exitMethodName(ctx: DeplorableParser.MethodNameContext?) {
    }

    override fun enterMethodArgument(ctx: DeplorableParser.MethodArgumentContext?) {
    }

    override fun exitMethodArgument(ctx: DeplorableParser.MethodArgumentContext?) {
    }

    override fun enterFunctionStatement(ctx: DeplorableParser.FunctionStatementContext?) {
    }

    override fun exitFunctionStatement(ctx: DeplorableParser.FunctionStatementContext?) {
    }

    override fun enterFunctionDeclaration(ctx: DeplorableParser.FunctionDeclarationContext?) {
    }

    override fun exitFunctionDeclaration(ctx: DeplorableParser.FunctionDeclarationContext?) {
    }

    override fun enterFunctionBody(ctx: DeplorableParser.FunctionBodyContext?) {
    }

    override fun exitFunctionBody(ctx: DeplorableParser.FunctionBodyContext?) {
    }

    override fun enterMetaX(ctx: DeplorableParser.MetaXContext?) {
    }

    override fun exitMetaX(ctx: DeplorableParser.MetaXContext?) {
    }
}