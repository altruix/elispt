package com.dpisarenko.deplorable

import com.dpisarenko.deplorable.grammar.DeplorableLexer
import com.dpisarenko.deplorable.grammar.DeplorableParser
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.tree.ParseTreeWalker

class Transpiler {
    fun run(deplorableScript:String):String {
        val lexer = DeplorableLexer(CharStreams.fromString(deplorableScript))
        val tokens = CommonTokenStream(lexer)
        val parser = DeplorableParser(tokens)
        val script: DeplorableParser.ScriptContext = parser.script()
        val walker = ParseTreeWalker()
        val listener = DeplorableScriptListener()
        walker.walk(listener, script)
        val deplorableStatements = listener.readDeplorableStatements
        val codeGenerator = EmacsLispCodeGenerator(deplorableStatements)
        return codeGenerator.transformToEmacsLisp()
    }
}