six-buffers() {
    evil-window-split();
    evil-window-vsplit();
    evil-window-vsplit();
    evil-window-down(1);
    evil-window-vsplit();
    evil-window-vsplit();
};
six-buffers();
