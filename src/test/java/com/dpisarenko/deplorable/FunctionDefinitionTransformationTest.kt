package com.dpisarenko.deplorable

import org.junit.Assert
import org.junit.Test

class FunctionDefinitionTransformationTest {
    @Test
    fun testTransformationOfFunctionDefinition() {
        // Given
        val deplorableScript = Utils.readFile("i5_function.deplorable.txt")
        val sut = Transpiler()

        // When
        val actualEmacsLispScript = sut.run(deplorableScript)

        // Then
        val expectedEmacsLispScript = Utils.readFile("i5_function.elisp.txt")
        Assert.assertEquals(expectedEmacsLispScript, actualEmacsLispScript)
    }
}