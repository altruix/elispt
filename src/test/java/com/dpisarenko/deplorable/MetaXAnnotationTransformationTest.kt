package com.dpisarenko.deplorable

import org.junit.Assert
import org.junit.Test

class MetaXAnnotationTransformationTest {
    @Test
    fun testTransformationOfEmptyMetaX() {
        // Given
        val deplorableScript = Utils.readFile("i6_metax.deplorable.txt")
        val sut = Transpiler()

        // When
        val actualEmacsLispScript = sut.run(deplorableScript)

        // Then
        val expectedEmacsLispScript = Utils.readFile("i6_metax.elisp.txt")
        Assert.assertEquals(expectedEmacsLispScript, actualEmacsLispScript)
    }
    @Test
    fun testTransformationOfMetaXWithArguments() {
        // Given
        val deplorableScript = Utils.readFile("i6_metax_with_args.deplorable.txt")
        val sut = Transpiler()

        // When
        val actualEmacsLispScript = sut.run(deplorableScript)

        // Then
        val expectedEmacsLispScript = Utils.readFile("i6_metax_with_args.elisp.txt")
        Assert.assertEquals(expectedEmacsLispScript, actualEmacsLispScript)
    }
}