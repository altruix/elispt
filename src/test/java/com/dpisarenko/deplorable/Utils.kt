package com.dpisarenko.deplorable

import java.io.File

class Utils {
    companion object {
        fun readFile(fileName:String):String {
            return File("src/test/resources/${fileName}").readText(Charsets.UTF_8)
        }
    }
}