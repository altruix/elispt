package com.dpisarenko.deplorable

import org.junit.Assert.assertEquals
import org.junit.Test

class MethodCallTransformationTest {
    @Test
    fun testTransformationOfMethodCall() {
        // Given
        // Script in deplorable language
        val deplorableScript = "message(\"This message appears in the echo area!\");" + System.lineSeparator()
        val sut = Transpiler()

        // When
        val emacsLispScript = sut.run(deplorableScript)

        // Then
        assertEquals("(message \"This message appears in the echo area!\")", emacsLispScript)
    }
    @Test
    fun testTransformationOfCallsToSplitWindowInSixParts() {
        // Given
        val deplorableScript = Utils.readFile("i4_sixScreens.deplorable.txt")
        val sut = Transpiler()

        // When
        val actualEmacsLispScript = sut.run(deplorableScript)

        // Then
        val expectedEmacsLispScript = Utils.readFile("i4_sixScreens.elisp.txt")
        assertEquals(expectedEmacsLispScript, actualEmacsLispScript)
    }
}